//
//  ChatFilterVc.swift
//  ServiceProvider
//
//  Created by Anubhav on 23/11/21.
//

import UIKit
import FittedSheets
class ChatFilterVc: UIViewController,Demoable {
    static var name: String = {"hgcycytwj"}()
    static let identifier = "ChatFilterVc"
    @IBOutlet weak var tableV:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "FilterByCell", bundle: nil), forCellReuseIdentifier: "FilterByCell")
       
     
        
    }
    static func openDemo(from parent: UIViewController, in view: UIView?) {
        let useInlineMode = view != nil
        
        let controller = UIStoryboard(name: "Service", bundle: nil).instantiateViewController(withIdentifier: "ChatFilterVc") as! ChatFilterVc
         
        let options = SheetOptions(
            useFullScreenMode: false,
            useInlineMode: useInlineMode)
        let sheet = SheetViewController(controller: controller, sizes: [.percent(0.6),.fullscreen], options: options)
//        sheet.hasBlurBackground = true
        sheet.overlayView.isOpaque = true
        sheet.gripSize = CGSize(width: 150, height: 8)
        sheet.minimumSpaceAbovePullBar = 100
        sheet.pullBarBackgroundColor = .white
//        sheet.dismissOnPull = false
        sheet.dismissOnOverlayTap = false
        addSheetEventLogging(to: sheet)
        
        if let view = view {
            sheet.animateIn(to: view, in: parent)
        } else {
            parent.present(sheet, animated: true, completion: nil)
        }
    }
   
}
    extension ChatFilterVc:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
           
                let cell = tableView.dequeueReusableCell(withIdentifier: "FilterByCell") as! FilterByCell
            cell.selectionStyle = .none
            if indexPath.row == 0{
                cell.lbl.textColor = UIColor(red: 245, green: 66, blue: 8)
           let img = UIImage(named: "ScreenShotCheck")
                img?.changeTintColor(color: UIColor(red: 245, green: 66, blue: 8))
                cell.imgCheck.image = img
            }else{
                let img = UIImage(named: "ScreenShotCheck")
                cell.imgCheck.image = img!.changeTintColor(color: .darkGray)
                
            }
                return cell
         
          
            
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        
    }



