//
//  ChatVc.swift
//  design
//
//  Created by satyam mac on 08/11/21.
//

import UIKit

class ChatVc: UIViewController {
    
    @IBOutlet weak var tableV:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "MassageRcell", bundle: nil), forCellReuseIdentifier: "MassageRcell")
        tableV.register(UINib(nibName: "MessegeSentCell", bundle: nil), forCellReuseIdentifier: "MessegeSentCell")
     
        
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
    extension ChatVc:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 2
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MessegeSentCell") as! MessegeSentCell
                return cell
               
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MassageRcell") as! MassageRcell
                
                return cell
            }
          
            
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        
    }



