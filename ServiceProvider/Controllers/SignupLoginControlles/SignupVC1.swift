//
//  SingupVC1.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 29/09/21.
//

import UIKit

class SingupVC1: UIViewController {
    
    
    @IBOutlet weak var BtnCornerRadius: UIButton!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        BtnCornerRadius.layer.cornerRadius = 20
        BtnCornerRadius.clipsToBounds = true
        
        BtnCornerRadius.layer.shadowRadius = 10
        BtnCornerRadius.layer.shadowOpacity = 1.0
        

       
    }
    

    
    @IBAction func btnback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func btnVC2c(_ sender: UIButton) {
        
        let nextVC2c = self.storyboard?.instantiateViewController(withIdentifier: "Signup2cVC") as! Signup2cVC
        self.navigationController?.pushViewController(nextVC2c, animated: true)
    }
    
    
    @IBAction func loginBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC3a") as! LoginVC3a
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
}

