//
//  ResetpasswordVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit

class ResetpasswordVC: UIViewController {

    @IBOutlet weak var btnsubmit: UIButton!
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        
        btnsubmit.layer.cornerRadius = 20
        btnsubmit.clipsToBounds = true
        
        btnsubmit.layer.shadowRadius = 10
        btnsubmit.layer.shadowOpacity = 1.0
        
        
        
    }
    
    
    @IBAction func btnback(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    

    @IBAction func btnDoyouVC(_ sender: UIButton) {
        
        let doyoucontroller = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! GradientTabBarController
        self.navigationController?.pushViewController(doyoucontroller, animated: true)
    }
    @IBAction func SignUpBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SingupVC") as! SingupVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
