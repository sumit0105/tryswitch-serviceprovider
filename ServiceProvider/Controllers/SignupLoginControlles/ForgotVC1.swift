//
//  ForgotVC1.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit

class ForgotVC1: UIViewController {
    
    @IBOutlet weak var btnCornerR: UIButton!
    @IBOutlet weak var bgView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        btnCornerR.layer.cornerRadius = 20
        btnCornerR.clipsToBounds = true
        
        btnCornerR.layer.shadowRadius = 10
        btnCornerR.layer.shadowOpacity = 1.0
        
    }
    

    @IBAction func btnResetPassword(_ sender: UIButton) {
        let reset = self.storyboard?.instantiateViewController(withIdentifier: "ResetpasswordVC") as! ResetpasswordVC
        
        self.navigationController?.pushViewController(reset, animated: true)
        
        
    }
    
    
    @IBAction func btnbackForgotvc(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func SignUpBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SingupVC") as! SingupVC
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
