//
//  Signup2cVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit

class Signup2cVC: UIViewController {
    
    @IBOutlet weak var btncornerradius: UIButton!
    @IBOutlet weak var bgView: UIView!

    override func viewDidLoad() {
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        super.viewDidLoad()
        btncornerradius.layer.cornerRadius = 20
        btncornerradius.clipsToBounds = true
        
        btncornerradius.layer.shadowRadius = 10
        btncornerradius.layer.shadowOpacity = 1.0
        

        // Do any additional setup after loading the view.
    }
    

 
    @IBAction func BtnBackVC1(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnsubmit(_ sender: Any) {
        let   submit =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC3a") as! LoginVC3a
        
        self.navigationController?.pushViewController(submit, animated: true)
        
    }
    
    
    @IBAction func btnLoginhere(_ sender: UIButton) {
        
        let login =  self.storyboard?.instantiateViewController(withIdentifier: "LoginVC3a") as! LoginVC3a
        
        self.navigationController?.pushViewController(login, animated: true)
    }
    
    
    
    @IBAction func loginBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC3a") as! LoginVC3a
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    

}
