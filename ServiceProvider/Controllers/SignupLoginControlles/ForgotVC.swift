//
//  ForgotVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 30/09/21.
//

import UIKit

class ForgotVC: UIViewController {
    
    
    @IBOutlet weak var btnClicknext: UIButton!
    @IBOutlet weak var bgView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        
        btnClicknext.layer.cornerRadius = 20
        btnClicknext.clipsToBounds = true
        
        btnClicknext.layer.shadowRadius = 10
        btnClicknext.layer.shadowOpacity = 1.0
    }
    @IBAction func btnNextForgotPassword(_ sender: UIButton) {
        let nextforgot = self.storyboard?.instantiateViewController(withIdentifier: "ForgotVC1") as! ForgotVC1
        
        self.navigationController?.pushViewController(nextforgot, animated: true)
    }
    
    
    @IBAction func btnbacklogin(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignupBtn(_ sender: UIButton) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SingupVC") as! SingupVC
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }
    
    
    

}
