//
//  SingupVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 29/09/21.
//

import UIKit

class SingupVC: UIViewController {

   
    @IBOutlet weak var BtnNext: UIButton!
    
    @IBOutlet weak var bgView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView.topRoundCorner(radius: 60, color: .white, borderWidth: 1)
        BtnNext.layer.cornerRadius = 20
        BtnNext.clipsToBounds = true
        
        BtnNext.layer.shadowRadius = 10
        BtnNext.layer.shadowOpacity = 1.0
        
        
    }
    
    
    @IBAction func btnNext(_ sender: UIButton) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SingupVC1") as! SingupVC1
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    
    @IBAction func loginBtn(_ sender: Any) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC3a") as! LoginVC3a
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
}
