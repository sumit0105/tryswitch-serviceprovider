//
//  PromoteVc.swift
//  design
//
//  Created by satyam mac on 09/11/21.
//

import UIKit

class PromoteVc: UIViewController {

    @IBOutlet weak var tableV:UITableView!
    @IBOutlet var viewRound: [UIView]!
    @IBOutlet weak var bottomAddView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "MyCampaignCell", bundle: nil), forCellReuseIdentifier: "MyCampaignCell")
        bottomAddView.roundCorners(radius: 23)
        self.tabBarController?.tabBar.isHidden = false
        for i in viewRound {
          
            i.setBorderColor(borderColor:UIColor(red: 245, green: 66, blue: 8), cornerRadiusBound: 18)
        }
     
        
    }
    @IBAction func btn_Add(_ sender: UIButton) {
        let vc = MyCompaignVc.instantiate(fromAppStoryboard: .Service)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func back_btn(_ sender: Any) {
        
        let doyoucontroller = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! GradientTabBarController
                self.navigationController?.pushViewController(doyoucontroller, animated: false)
    }
}
    extension PromoteVc:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 5
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
           
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyCampaignCell") as! MyCampaignCell
            cell.selectionStyle = .none
                return cell
         
          
            
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        
    }



