//
//  PreviewVc.swift
//  design
//
//  Created by satyam mac on 10/11/21.
//

import UIKit

class PreviewVc: UIViewController {

    @IBOutlet weak var tableV:UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "PreviewCell", bundle: nil), forCellReuseIdentifier: "PreviewCell")
     
     
        
    }
    @IBAction func btn_Add(_ sender: UIButton) {
       
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
    extension PreviewVc:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
           
                let cell = tableView.dequeueReusableCell(withIdentifier: "PreviewCell") as! PreviewCell
            cell.selectionStyle = .none
            cell.payAction = {
                UserDefaults.setObject("Promote", forKey: "Promote")
                let doyoucontroller = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! GradientTabBarController
                doyoucontroller.selectedIndex = 0
                        self.navigationController?.pushViewController(doyoucontroller, animated: false)
                
            }
            cell.backAction = {
                self.navigationController?.popViewController(animated: true)
                
            }
                return cell
         
          
            
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
        
    }



