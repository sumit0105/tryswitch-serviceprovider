//
//  MyCompaignVc.swift
//  ServiceProvider
//
//  Created by satyam mac on 12/11/21.
//

import UIKit
import DropDown

class MyCompaignVc: UIViewController {

    @IBOutlet weak var serviceTf: UITextField!
    let drop_IdType = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.drop_IdType
        ]
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func dropDownBtn(_ sender: UIButton) {
        self.drop_IdType.anchorView = sender
        self.drop_IdType.bottomOffset = CGPoint(x: 0, y: sender.bounds.height - 4 + 0)
        self.drop_IdType.textColor = .black
//        self.drop_IdType.s
        self.drop_IdType.separatorColor = .clear
        self.drop_IdType.selectionBackgroundColor = .clear
        self.drop_IdType.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.drop_IdType.dataSource.removeAll()
        drop_IdType.cornerRadius = 12
        self.drop_IdType.cellHeight = 35
        self.drop_IdType.dataSource.append(contentsOf: ["Item 1","Item 2","Item 3","Item 4"])
        
        self.drop_IdType.selectionAction = { [unowned self] (index, item) in
            self.serviceTf.text = item
        }
        self.drop_IdType.show()
        
    }
    
    
    @IBAction func backAnddismis(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func CompaignBtn(_ sender: Any) {
        let vc = PreviewVc.instantiate(fromAppStoryboard: .Service)
        self.navigationController?.pushViewController(vc, animated: true)
    } 

}
