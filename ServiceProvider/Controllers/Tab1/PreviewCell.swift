//
//  PreviewCell.swift
//  design
//
//  Created by satyam mac on 10/11/21.
//

import UIKit

class PreviewCell: UITableViewCell {
    var backAction:()->() = {}
    var payAction:()->() = {}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func pay(_ sender: Any) {
        payAction()
    }
    @IBAction func back(_ sender: Any){
        backAction()
        
    }
    
}
