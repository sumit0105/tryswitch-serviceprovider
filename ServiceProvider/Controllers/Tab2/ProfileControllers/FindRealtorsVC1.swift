//
//  FindRealtorsVC.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 12/10/21.
//

import UIKit

class FindRealtorsVC1: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblView.delegate = self
        self.tblView.dataSource = self
       
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FindRealtorsCell1
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        130
    }
}
