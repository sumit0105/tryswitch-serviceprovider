//
//  ServiceOfferingVc.swift
//  ServiceProvider
//
//  Created by satyam mac on 12/11/21.
//

import UIKit

class ServiceOfferingVc: UIViewController {
    
    @IBOutlet weak var tableV:UITableView!
    @IBOutlet weak var bgView1: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        bgView1.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
        tableV.delegate = self
        tableV.dataSource = self
        tableV.register(UINib(nibName: "ServiceOffringsCell", bundle: nil), forCellReuseIdentifier: "ServiceOffringsCell")
        bgView1.roundCornersBottomright_BottomLeft(radius: 20)
//        tableV.register(UINib(nibName: "BottomButton", bundle: nil), forCellReuseIdentifier: "BottomButton")
        
     
        
    }
  
    @IBAction func addService(_ sender: Any) {
        let vc = AddServiceVc.instantiate(fromAppStoryboard: .Service)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ServiceOfferingVc:UITableViewDelegate,UITableViewDataSource{
        func tableView (_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceOffringsCell") as! ServiceOffringsCell
//            if indexPath.row == 3{
//                let cell = tableView.dequeueReusableCell(withIdentifier: "BottomButton") as! BottomButton
//                cell.serviceAction = {
//                    
//                }
//                return cell
//            }
//            cell.btnOpenClose = indexPath
            
           
//            cell.btnOpenClose.addTarget(self, action: #selector(open(_:)), for: .touchUpInside)
            return cell
            }
          
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if let cell = tableView.cellForRow(at: indexPath) as? ServiceOffringsCell {
                
                UIView.animate(withDuration: 0.3) {
                    cell.bottomView.isHidden = !cell.bottomView.isHidden
                }
                if cell.bottomView.isHidden {
                    cell.btnOpenClose.setTitle("+", for: .normal)
                }else{
                    cell.btnOpenClose.setTitle("-", for: .normal)
                }
                tableView.beginUpdates()
                tableView.endUpdates()
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }

            
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            UITableView.automaticDimension
        }
    
//    @IBAction func open(_ sender: UIButton){
//        if let cell = tableView.cellForRow(at: sender.tag) as? ServiceOffringsCell {
//
//        }
//    }
        
    }



