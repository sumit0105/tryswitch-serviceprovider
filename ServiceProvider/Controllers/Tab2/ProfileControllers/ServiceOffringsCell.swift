//
//  ServiceOffringsCell.swift
//  ServiceProvider
//
//  Created by satyam mac on 12/11/21.
//

import UIKit

class ServiceOffringsCell: UITableViewCell {

    @IBOutlet weak var btnOpenClose: UIButton!
    @IBOutlet public weak var bottomView: UIView! {
        didSet {
            bottomView.isHidden = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
