//
//  SubCategoryCollCell.swift
//  design
//
//  Created by Anubhav on 11/11/21.
//

import UIKit

class SubCategoryCollCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.setBorderColor(borderColor: .lightGray, cornerRadiusBound: 20)
        // Initialization code
    }

}
