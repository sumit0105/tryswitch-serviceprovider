//
//  ProfileContainerVC.swift
//  Realtor
//
//  Created by satyam mac on 31/10/21.
//

import UIKit
//import CarbonKit
import MXSegmentedControl
class ProfileContainerVC: UIViewController {
   
    @IBOutlet weak var lblTrel: NSLayoutConstraint!
    @IBOutlet weak var Slead: NSLayoutConstraint!
    
    @IBOutlet weak var lblLead: NSLayoutConstraint!
    @IBOutlet weak var Strail: NSLayoutConstraint!
    @IBOutlet weak var leadCons: NSLayoutConstraint!
    @IBOutlet weak var tralCons: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bgView1: UIView!
    @IBOutlet var segmentedControl3: MXSegmentedControl!
//    @IBOutlet private weak var HomeBtnView: UIStackView!
//    @IBOutlet private weak var btn_profile: UIButton!
//    @IBOutlet private weak var btn_Buisness: UIButton!
//    @IBOutlet private weak var btn_Review: UIButton!
//    @IBOutlet private weak var btn_profileLine: UILabel!
//    @IBOutlet private weak var btn_BuisnessLine: UILabel!
//    @IBOutlet private weak var btn_ReviewLine: UILabel!
    var profile:ProfileVc! = nil
    var Buisness:ProfileVc! = nil
    var Review:FindRealtorsVC1! = nil
     var check:Bool = true
    
//    @IBOutlet weak var containerView: UIView!
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    
    override func viewDidLoad() {
        
        segmentedControl3.append(title: "Profile Information")
            .set(titleColor:UIColor(red: 245, green: 66, blue: 8), for: .selected)
        segmentedControl3.append(title: "Business Information")
            .set(titleColor:UIColor(red: 245, green: 66, blue: 8), for: .selected)
        segmentedControl3.append(title: "Service Offerings")
            .set(titleColor:UIColor(red: 245, green: 66, blue: 8), for: .selected)
        segmentedControl3.append(title: "Reviews")
            .set(titleColor:UIColor(red: 245, green: 66, blue: 8), for: .selected)
        segmentedControl3.addTarget(self, action: #selector(changeIndex(segmentedControl:)), for: .valueChanged)
        currentPage = 0
//        createPageViewController()
//        self.btnClicked(btn: self.btn_profile)
//        var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
//        super.viewDidLoad()
//
//        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: ["Profile Information","Business Information","Reviews"], delegate: self)
////         carbonTabSwipeNavigation.view.frame = self.containerView.bounds
//        containerView.addSubview(carbonTabSwipeNavigation.view)
//         addChild(carbonTabSwipeNavigation)
//        carbonTabSwipeNavigation.view.backgroundColor = .clear
//         carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3 + 30, forSegmentAt: 0 )
//
//
//         carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3 + 30, forSegmentAt: 1 )
//
//         carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(view.frame.width / 3, forSegmentAt: 2 )
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
        bgView1.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
//        carbonTabSwipeNavigation.setTabBarHeight(44)
//
////        carbonTabSwipeNavigation.setIndicatorColor(UIColor.darkGray)
//
////        carbonTabSwipeNavigation.setNormalColor(.darkGray)
//        carbonTabSwipeNavigation.setSelectedColor(UIColor.blue)
//        carbonTabSwipeNavigation.setCurrentTabIndex(0, withAnimation: true)
//        carbonTabSwipeNavigation.setIndicatorColor(UIColor.red)
//
        
        
//        carbonTabSwipeNavigation.carbonSegmentedControl?.backgroundColor = UIColor.clear
        
        
    }
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
        print(segmentedControl.selectedIndex)
        
        if segmentedControl.selectedIndex == 2 {
            bgView1.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
            bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
            leadCons.constant = 5
            tralCons.constant = 5
            Slead.constant = 5
            Strail.constant = 5
            lblLead.constant = 5
            lblTrel.constant = 5
        }else{
            bgView1.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)
            leadCons.constant = 0
            tralCons.constant = 0
            Strail.constant = 0
            Slead.constant = 0
            
            lblLead.constant = 0
            lblTrel.constant = 0
            bgView.addShadow(shadowColor: UIColor.white, offSet: CGSize(width: 0, height: 0), opacity: 0, shadowRadius: 0)
        }
        if let segment = segmentedControl.segment(at: segmentedControl.selectedIndex) {
            segmentedControl.indicator.boxView.backgroundColor = segment.titleColor(for: .selected)
            segmentedControl.indicator.lineView.backgroundColor = segment.titleColor(for: .selected)
        }
    }
    
//    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
//        guard let storybord  = storyboard else { return UIViewController() }
//        let storyB = UIStoryboard(name: "PopUp", bundle: nil)
//        if index == 0{
//
//            return storyB.instantiateViewController(withIdentifier: "ChangePasswordVc")
//        }
//        else if index == 1 {
//
//            return storyB.instantiateViewController(withIdentifier: "ChangePasswordVc")
//
//
//        }
//
//        else{
//            return storyB.instantiateViewController(withIdentifier: "ChangePasswordVc")
//        }
//
//
//    }
    private func selectedButton(btn: UIButton,viewBack:UIView) {
        
        btn.setTitleColor(#colorLiteral(red: 0.9975497127, green: 0.3389877081, blue: 0.1659283638, alpha: 1), for: .normal)
        viewBack.backgroundColor = #colorLiteral(red: 0.9975497127, green: 0.3389877081, blue: 0.1659283638, alpha: 1)
    }
    private func unSelectedButton(btn: UIButton,viewBack:UIView) {
        btn.setTitleColor(#colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1), for: .normal)
        viewBack.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                
    }
    
    @IBAction private func btnClicked(btn: UIButton) {
        
        pageController.setViewControllers([arrVC[btn.tag-1]], direction: UIPageViewController.NavigationDirection.reverse, animated: false, completion: {(Bool) -> Void in
        })
        
//        resetTabBarForTag(tag: btn.tag-1)
    }

    @IBAction func back_btn(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    
}
//extension ProfileContainerVC: UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate{
////    private func createPageViewController() {
//
//        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
//
//        pageController.view.backgroundColor = UIColor.clear
//        pageController.delegate = self
//        pageController.dataSource = self
//
//        for svScroll in pageController.view.subviews as! [UIScrollView] {
//            svScroll.delegate = self
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            let poi = self.btn_profile.frame.size.height
//            // let frame = self.view.convert(self.view.frame, from:self.btnTab1)
//            let frame = self.view.convert(self.btn_Buisness.frame, from:self.HomeBtnView)
//
//            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
//                (self.navigationController?.navigationBar.frame.height ?? 0.0)
//            print(topBarHeight)
//            // CGPoint point = [subview1 convertPoint:subview2.frame.origin toView:self.view];
//
//            // self.pageController.view.frame = CGRect(x: 0, y: poi + frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height - poi - 70 - 60)
//
//            self.pageController.view.frame = CGRect(x: 0, y: poi + frame.origin.y + 8 , width: self.view.frame.size.width, height: self.view.frame.size.height - poi - frame.origin.y - 8)
//
//        }
//
//        let storyB = UIStoryboard(name: "PopUp", bundle: nil)
//        let storyB1 = UIStoryboard(name: "SignUp", bundle: nil)
//        guard let profilevc = UserDefaults.standard.object(forKey: "pfc") else {
//            return
//        }
//        print(profilevc,"------------")
//        profile = storyB.instantiateViewController(withIdentifier: profilevc as! String) as? ProfileVc
//
//        Buisness = storyB.instantiateViewController(withIdentifier: "BuisnessVc") as? ProfileVc
//        Review = storyB1.instantiateViewController(withIdentifier: "FindRealtorsVC1") as? FindRealtorsVC1
//
//        arrVC = [profile,Buisness,Review]
//
//        pageController.setViewControllers([profile], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
//
//        self.addChild(pageController)
//        self.view.addSubview(pageController.view)
//        pageController.didMove(toParent: self)
//    }
//    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
//        if(arrVC .contains(viewCOntroller)) {
//            return arrVC.firstIndex(of: viewCOntroller)!
//        }
//
//        return -1
//    }
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
//
//        var index = indexofviewController(viewCOntroller: viewController)
//
//        if(index != -1) {
//            index = index - 1
//            //            view_left.isHidden = false
//            //            view_right.isHidden = true
//        }
//
//        if(index < 0) {
//            return nil
//        }
//        else {
//            return arrVC[index]
//        }
//
//    }
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//
//        var index = indexofviewController(viewCOntroller: viewController)
////
////        if(index != -1) {
////            index = index + 1
////            //            view_right.isHidden = false
////            //            view_left.isHidden = true
////        }
////
////        if(index >= arrVC.count) {
////            return nil
////        }
////        else {
////            return arrVC[index]
////        }
////
////    }
//    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
//
//        if(completed) {
//            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
//            resetTabBarForTag(tag: currentPage)
//        }
//    }
//    private func resetTabBarForTag(tag: Int) {
//
//        var sender: UIButton!
//        var selectedView : UIView!
//
//        if(tag == 0) {
//            sender = btn_profile
//            selectedView = btn_profileLine
//
//        }else if(tag == 1) {
//            sender = btn_Buisness
//            selectedView = btn_BuisnessLine
//        }else if(tag == 2) {
//            sender = btn_Review
//            selectedView = btn_ReviewLine
//
//        }
//        currentPage = tag
//
//        unSelectedButton(btn: btn_profile, viewBack: btn_profileLine)
//        unSelectedButton(btn: btn_Buisness, viewBack: btn_BuisnessLine)
//        unSelectedButton(btn: btn_Review, viewBack: btn_ReviewLine)
//        selectedButton(btn: sender, viewBack: selectedView)
//
//    }
//}
