//
//  EditProfileVc.swift
//  design
//
//  Created by satyam mac on 27/10/21.
//

import UIKit
import DropDown
protocol profileShow {
   func showDetails()
}
class EditProfileVc: UIViewController {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var hotsopView: UIView!
    var delegate:profileShow!
    @IBOutlet weak var categoryTf: UITextField!
    @IBOutlet weak var SubcategoryTf: UITextField!
    let drop_IdType = DropDown()
    
    lazy var dropDowns: [DropDown] = {
        return [
            self.drop_IdType
        ]
    }()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        hotsopView.frame = self.view.frame
        self.view.addSubview(self.hotsopView)
        hotsopView.isHidden = true
        collView.dataSource = self
        collView.delegate = self
        collView.register(UINib(nibName: "SubCategoryCollCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryCollCell")
        bgView.addShadow(shadowColor: UIColor.black, offSet: CGSize(width: 1, height: 1), opacity: 0.3, shadowRadius: 5.0)

       
    }
    @IBAction func dropDownBtn(_ sender: UIButton) {
        self.drop_IdType.anchorView = sender
        self.drop_IdType.bottomOffset = CGPoint(x: 0, y: sender.bounds.height - 4 + 0)
        self.drop_IdType.textColor = .black
//        self.drop_IdType.s
        self.drop_IdType.separatorColor = .clear
        self.drop_IdType.selectionBackgroundColor = .clear
        self.drop_IdType.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.drop_IdType.dataSource.removeAll()
        drop_IdType.cornerRadius = 12
        self.drop_IdType.cellHeight = 35
        self.drop_IdType.dataSource.append(contentsOf: ["Item 1","Item 2","Item 3","Item 4"])
        
        self.drop_IdType.selectionAction = { [unowned self] (index, item) in
            self.categoryTf.text = item
        }
        self.drop_IdType.show()
        
    }
    @IBAction func SubdropDownBtn(_ sender: UIButton) {
        self.drop_IdType.anchorView = sender
        self.drop_IdType.bottomOffset = CGPoint(x: 0, y: sender.bounds.height - 4 + 0)
        self.drop_IdType.textColor = .black
//        self.drop_IdType.s
        self.drop_IdType.separatorColor = .clear
        self.drop_IdType.selectionBackgroundColor = .clear
        self.drop_IdType.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.drop_IdType.dataSource.removeAll()
        drop_IdType.cornerRadius = 12
        self.drop_IdType.cellHeight = 35
        self.drop_IdType.dataSource.append(contentsOf: ["Item 1","Item 2","Item 3","Item 4"])
        
        self.drop_IdType.selectionAction = { [unowned self] (index, item) in
            self.categoryTf.text = item
        }
        self.drop_IdType.show()
        
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func hotspot_Dismis(_ sender: Any) {
        self.hotsopView.isHidden = true
    }
    
    @IBAction func hotspot_Ok(_ sender: Any) {
        self.hotsopView.isHidden = true
        self.delegate.showDetails()
        self.navigationController?.popViewController(animated: false)
        
    }
    @IBAction func submitBtn(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ProfileApprovalVc") as! ProfileApprovalVc
//        self.navigationController?.pushViewController(vc, animated:
//        false)
        self.hotsopView.isHidden = false
        
        
    }
    
}
extension EditProfileVc:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCollCell", for: indexPath) as! SubCategoryCollCell
       
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let size = CGSize(width: (collectionView.bounds.size.width/2)-5, height: 40)


        return size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
}
