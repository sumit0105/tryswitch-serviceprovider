//
//  BottomButton.swift
//  ServiceProvider
//
//  Created by satyam mac on 12/11/21.
//

import UIKit

class BottomButton: UITableViewCell {
    var serviceAction:()->() = {}
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func serviceOfferdBtn(_ sender: Any) {
        serviceAction()
    }
}
