//
//  FindrealtorsChatlist.swift
//  TRYSWITCH
//
//  Created by YATIN  KALRA on 13/10/21.
//

import UIKit

class FindrealtorsChatlist: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var TblView : UITableView!
    
    var arrayimg: [UIImage] = [#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12"),#imageLiteral(resourceName: "12")]
    var arraylbl1 = ["John Smith","Michel Lonard","Linea Martin","John Smith","Michel Lonard","Linea Martin","Michel Lonard"]
    var arraylbl2 = ["You: hello","Yes, Want your seruice for my ne","You: Hello","You: hello","Yes, Want your seruice for my ne...","You : hello","You: helo"]
    var arraylbl3 = ["Realtor","Service","Service","Realtor","Service","Realtor","Service"]
    var arraylbl4 = ["2H ago","Aug 2","Aug 2","2H ago","Aug 2","Aug 2","2H ago"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.TblView.delegate  = self
        self.TblView.dataSource = self

        
    }
    @IBAction func back_btn(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func chatBoxBtn(_ sender: Any) {
        ChatFilterVc.openDemo(from: self, in: self.view)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayimg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ChatTableViewCell
        cell.img.image = arrayimg[indexPath.row]
        cell.lbl1.text = arraylbl1[indexPath.row]
        cell.lbl2.text = arraylbl2[indexPath.row]
        cell.lbl3.text = arraylbl3[indexPath.row]
        cell.lbl4.text = arraylbl4[indexPath.row]
    
        cell.selectionStyle = .none
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             let vc = ChatVc1.instantiate(fromAppStoryboard: .Service)
               self.navigationController?.pushViewController(vc, animated:true)
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        120
    }
    
}
extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
