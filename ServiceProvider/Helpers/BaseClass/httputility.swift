//
//  httputility.swift

//
//  Created by satyam mac on 03/12/20.
//  Copyright © 2020 satyam mac. All rights reserved.
//

import Foundation


struct HttpUtility {
    func getApiData<T:Decodable>(requestUrl: URL, resultType: T.Type, completionHandler:@escaping(_ result: T)-> Void)
    {
        URLSession.shared.dataTask(with: requestUrl) { (responseData, httpUrlResponse, error) in

            if(error == nil && responseData != nil && responseData?.count != 0)
            {
                //parse the responseData here
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(T.self, from: responseData!)
                    _=completionHandler(result)
                }
                catch let error{
                    debugPrint("error occured while decoding = \(error.localizedDescription)")
                }
            }

        }.resume()
    }
}

func PostData<T:Decodable>(requestUrl:URL,resPonseBody:Data,resultType:T.Type, complitionHandler:@escaping(_ result:T) -> Void)  {

    var urlRequest = URLRequest(url: requestUrl)
    urlRequest.httpBody = resPonseBody
    urlRequest.httpMethod = "post"
    urlRequest.addValue("aplication/json", forHTTPHeaderField: "content-type")
    
    URLSession.shared.dataTask(with: urlRequest){(data,httpUrlresponse,error) in
        if (data == nil && data?.count != 0){
            let decoder = JSONDecoder()
            do {
                let urlresPONSE = try decoder.decode(T.self, from: data!)
                _=complitionHandler(urlresPONSE)
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }.resume()
        
    }



