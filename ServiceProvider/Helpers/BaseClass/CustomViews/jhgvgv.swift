//
//  AvailbableBookingPopUpController.swift
//  GsgMechanic
//
//  Created by satyam mac on 17/03/21.
//

//import UIKit
//import EMTNeumorphicView
//class AvailbableBookingPopUpController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
//
//
//
//
//    @IBOutlet weak var dismissView: UIView!
//
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var headLineView: UIView!
//    @IBOutlet weak var PopUpBaseView: UIView!
//    @IBOutlet weak var profileImg: UIImageView!
//    @IBOutlet weak var profileBg: EMTNeumorphicView!
//    @IBOutlet weak var serviceTableBg: UITableView!
//    @IBOutlet weak var acceptBtn: EMTNeumorphicButton!
//    @IBOutlet weak var serviceBg: EMTNeumorphicView!
//    @IBOutlet weak var rejectBtn: EMTNeumorphicButton!
//    @IBOutlet weak var contentPopupView: UIView!
//
//    @IBOutlet weak var personDetailView: UIView!
//
//    override func viewDidLoad(){
//        super.viewDidLoad()
//
//
//        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
//        self.dismissView.addGestureRecognizer(gesture)
//
//
//        headLineView.topRoundCorner(radius: 34, color: UIColor(RGB: 0), borderWidth: 0)
//        PopUpBaseView.topRoundCorner(radius: 34, color: UIColor(RGB: 2), borderWidth: 0)
//        contentView.topRoundCorner(radius: 34, color: UIColor(RGB: 2), borderWidth: 0)
//
//
//        //adding an overlay to the view to give focus to the dialog box
//
//        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
//        dismissView.backgroundColor = UIColor.black.withAlphaComponent(0.50)
//        setupLayout ()
//
//    }
//
//    func setupLayout (){
//        contentPopupView.backgroundColor = UIColor(RGB: 0xF0EEEF)
//        personDetailView.backgroundColor = UIColor(RGB: 0xF0EEEF)
//        serviceBg.neumorphicLayer?.elementBackgroundColor = contentView.backgroundColor!.cgColor
//        serviceBg.neumorphicLayer?.depthType = .convex
//        serviceBg.neumorphicLayer?.cornerRadius = 20
//        serviceBg.neumorphicLayer?.edged = true
//        serviceBg.neumorphicLayer?.darkShadowOpacity = 0.4
//        serviceBg.neumorphicLayer?.lightShadowOpacity = 1
//        acceptBtn.neumorphicLayer?.elementBackgroundColor = contentView.backgroundColor!.cgColor
//        acceptBtn.neumorphicLayer?.depthType = .convex
//        acceptBtn.neumorphicLayer?.cornerRadius = 5
//        acceptBtn.neumorphicLayer?.edged = true
//        rejectBtn.neumorphicLayer?.elementBackgroundColor = contentView.backgroundColor!.cgColor
//        rejectBtn.neumorphicLayer?.depthType = .convex
//        rejectBtn.neumorphicLayer?.cornerRadius = 5
//        rejectBtn.neumorphicLayer?.edged = true
//
//    }
//
//
//    @objc func someAction(_ sender:UITapGestureRecognizer){
//
//        self.dismiss(animated: true, completion: nil)
//       }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 10
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "bookservicecell") as! BookSerivceCell
//        return cell
//    }
//
//
//    //MARK:- functions for the viewController
//
//
//    static func showPopup(parentVC: UIViewController){
//
//        //creating a reference for the dialogView controller
//
//        if let popupViewController = UIStoryboard(name: "CustomView", bundle: nil).instantiateViewController(withIdentifier: "availablebooking") as? AvailbableBookingPopUpController {
//            popupViewController.modalPresentationStyle = .custom
//           popupViewController.modalTransitionStyle = .crossDissolve
//
//            //setting the delegate of the dialog box to the parent viewController
////            popupViewController.delegate = parentVC as? PopUpProtocol
//
//            //presenting the pop up viewController from the parent viewController
//
//            parentVC.present(popupViewController, animated: true)
//
//        }
//    }
//}
////class BookSerivceCell: UITableViewCell {
////    @IBOutlet weak var serviceLabel : UILabel!
////    @IBOutlet weak var rupeeLabel : UILabel!
////    @IBOutlet weak var checkBoxButton : EMTNeumorphicButton!
////
//    // MARK: Initalizers
////}
