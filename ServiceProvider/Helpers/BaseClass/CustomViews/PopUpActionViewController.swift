//
//  PopupActionViewController.swift
//  CustomDialogBox
//
//  
//

import UIKit

//Protocol to inform the Parent viewController to take some action based on the dialog box
protocol PopUpProtocol {
    func handleAction(action: Bool)
}
private enum AnimationExampleType: String {
    case popIn = "popIn()"
}
private let AllExamples: [AnimationExampleType] = [.popIn]

class PopUpActionViewController: UIViewController {
    
    static let identifier = "PopUpActionViewController"
    
    var delegate: PopUpProtocol?
    
    //MARK:- outlets for the view controller
    @IBOutlet weak var dialogBoxView: UIView!
   @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var imgStautus: UIImageView?
    @IBOutlet var btnView: UIView!
    @IBOutlet var btnTitleLbl: UILabel!
    var actionBool:Bool!
    var string:String!
    var img:UIImage!
    var btnBacground:UIColor!
    var btnTitleDs:String!
    //MARK:- lifecyle methods for the view controller
    override func viewDidLoad() {
        super.viewDidLoad()
        btnView.setBorderColor(borderColor: .darkGray, cornerRadiusBound: 10)
    view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        
        //customizing the dialog box view
        dialogBoxView.layer.cornerRadius = 10.0

        
        self.statusLbl.text = string
        self.imgStautus?.image = img
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        self.btnView.addGestureRecognizer(gesture)

//        self.btnView.backgroundColor = btnBacground
        
        self.btnTitleLbl.text = btnTitleDs
    }
    @objc func someAction(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
        self.delegate?.handleAction(action: actionBool)
        }

    private func startAnimation(_ type: AnimationExampleType) {
        switch (type) {
        case .popIn:
            dialogBoxView.popIn()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.dialogBoxView.popIn()
    }
    
    //MARK:- outlet functions for the viewController

    

    
    //MARK:- functions for the viewController
    static func showPopup(parentVC: UIViewController,statuslbl:String,img:UIImage,btnBacground:UIColor,btnTitleDs:String,actionBool:Bool){
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpActionViewController") as? PopUpActionViewController {
            popupViewController.modalPresentationStyle = .custom
           popupViewController.modalTransitionStyle = .crossDissolve
            popupViewController.string = statuslbl
            popupViewController.img = img
            popupViewController.btnBacground = btnBacground
            popupViewController.btnTitleDs = btnTitleDs
            popupViewController.actionBool = actionBool
            //setting the delegate of the dialog box to the parent viewController
            popupViewController.delegate = parentVC as? PopUpProtocol

            //presenting the pop up viewController from the parent viewController
            
            parentVC.present(popupViewController, animated: true)
            
        }
    }
    
}
