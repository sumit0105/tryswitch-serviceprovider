//
//  Validators.swift
//  TestVl
//
//  Created by Mac User on 04/11/20.
//  Copyright © 2020 ABC. All rights reserved.
//

import Foundation

class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}

enum ValidatorType {
    case username
    case email
    case mobile
    case password
    case confirmpassword(field: String)
    case pancard

    
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .username: return UserNameValidator()
        case .email: return EmailValidator()
        case .mobile: return MobileValidator()
        case .password: return PasswordValidator()
        case .confirmpassword(let fieldName): return RequiredFieldValidator(fieldName)
        case .pancard: return PanCardValidator()

        }
    }
}


struct UserNameValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Please enter Name")}
        
         guard value.count >= 3 else {
         throw ValidationError("Name must contain more than three characters" )
         }
         guard value.count < 18 else {
         throw ValidationError("Name shoudn't conain more than 18 characters" )
         }
         
         do {
         if try NSRegularExpression(pattern: "^(([^ ]?)(^[a-zA-Z].*[a-zA-Z]$)([^ ]?))$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
         throw ValidationError("Invalid Name, numbers or special characters not allowed")
         }
         } catch {
         throw ValidationError("Invalid Name, numbers or special characters not allowed")
         }
         return value
    }
}

struct PanCardValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Please enter Pan Number")}
        
         do {
         if try NSRegularExpression(pattern: "^[A-Z]{5}[0-9]{4}[A-Z]{1}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
         throw ValidationError("Invalid Pan, Pan should not contain whitespaces, numbers or special characters")
         }
         } catch {
         throw ValidationError("Not a valid Pan Number")
         }
         return value
    }
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Please enter e-mail Address")}
        
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid e-mail Address")
            }
        } catch {
            throw ValidationError("Invalid e-mail Address")
        }
        return value
    }
}

struct MobileValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Please enter mobile number")}
        do {
            if try NSRegularExpression(pattern: "^[7-9][0-9]{9}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid mobile number")
            }
        } catch {
            throw ValidationError("Invalid mobile number")
        }
        return value
    }
}

struct PasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Password is Required")}
        guard value.count >= 6 else { throw ValidationError("Password must have at least 6 characters") }
        
//        do {
//            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
//                throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
//            }
//        } catch {
//            throw ValidationError("Password must be more than 6 characters, with at least one character and one numeric character")
//        }
        return value
    }
}


struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
        
    }
    
    func validated(_ value: String) throws -> String {
        guard !value.isEmpty else {
            throw ValidationError("Confirm Password is required")
        }
        return value
    }
}


