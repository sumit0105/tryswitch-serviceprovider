//
//  ProjectUtils.swift
//  QuizAdda
//
//  Created by Inmorteltech on 26/03/18.
//  Copyright © 2018 Yogendra Singh. All rights reserved.
//

import UIKit

//
//  ProjectUtils.swift
//  QuizAdda
//
//  Created by Inmorteltech on 26/03/18.
//  Copyright © 2018 Yogendra Singh. All rights reserved.
//

import UIKit


class ProjectUtils: NSObject {
    
    
    //  used in lauch
    class func saveUserLogin(Value:String)
    {
        UserDefaults.standard.setValue(Value, forKey: "IsUserLoggedIn")
    }
    
    class func getUserLoggedIn() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "IsUserLoggedIn") as! String?
        if Userval == nil {
            return "No"
        }
        else{
            return Userval
        }
    }
    
    class func saveUserName(userName:String)
    {
        UserDefaults.standard.setValue(userName, forKey: "userName")
    }
    
    class func getUserName() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "userName") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveOTP(OTP:String)
    {
        UserDefaults.standard.setValue(OTP, forKey: "OTP")
    }
    
    class func getOTP() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "OTP") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func savefullUserName(fullUserName:String)
    {
        UserDefaults.standard.setValue(fullUserName, forKey: "fullUserName")
    }
    
    class func getfullUserName() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "fullUserName") as! String?
        if Userval == nil {
            return "No"
        }
        else{
            return Userval
        }
    }
    
    class func saveLoginType(LoginType:String)
    {
        UserDefaults.standard.setValue(LoginType, forKey: "LoginType")
    }
    
    class func getLoginType() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "LoginType") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveEmailID(email_id:String)
    {
        UserDefaults.standard.setValue(email_id, forKey: "email_id")
    }
    
    class func getEmailID() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "email_id") as! String?
        if Userval == nil {
            return "No"
        }
        else{
            return Userval
        }
    }
    
    class func saveUserID(UserID:String)
    {
        UserDefaults.standard.setValue(UserID, forKey: "UserID")
    }
    
    class func getUserID() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "UserID") as! String?
        if Userval == nil {
            return "No"
        }
        else{
            return Userval
        }
    }
    
    // used in register
    class func saveUuidId(ID: String)  {
        UserDefaults.standard.setValue(ID, forKey: "uuid")
    }
    class func getUuidId() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:String! = userdefaults.object(forKey: "uuid") as! String?
        if userData == nil{
            return "No"
        }else{
            return userData
        }
    }
    
    // used in login
    class func saveAcesstoken(ID: String)  {
        UserDefaults.standard.setValue(ID, forKey: "Acesstoken")
    }
    class func getAcesstoken() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:String! = userdefaults.object(forKey: "Acesstoken") as! String?
        return userData
    }
    
    //  used in login
    class func saveUserDict(Username: NSDictionary)  {
        UserDefaults.standard.setValue(Username, forKey: "UserDict")
    }
    
    
    class func getUserDict() -> NSDictionary
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:NSDictionary! = userdefaults.object(forKey:"UserDict") as! NSDictionary?
        return (userData)!
    }
    
    // used in register
    class func saveAddressDict(Username: NSDictionary)  {
        UserDefaults.standard.setValue(Username, forKey: "Address")
    }
    
    
    class func getAddressDict() -> NSDictionary
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:NSDictionary! = userdefaults.object(forKey:"Address") as! NSDictionary?
        return (userData)!
    }
    
    // used in login
    class func saveLoginCredentialInBase64(ID: String)  {
        UserDefaults.standard.setValue(ID, forKey: "LoginCredential")
    }
    
    class func getLoginCredentialInBase64() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:String! = userdefaults.object(forKey: "LoginCredential") as? String
        return userData
    }
    
    //  used in lauch
    class func isUserEnterLoginDetails(Value:String)
    {
        UserDefaults.standard.setValue(Value, forKey: "IsUserEnterMobile")
    }
    
    class func getUserLoginDetailsCheck() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "IsUserEnterMobile") as! String?
        if Userval == nil {
            return "No"
        }
        else{
            return Userval
        }
    }
    
    // used in login
    class func saveLoginDetails(LoginDict:NSDictionary)  {
        UserDefaults.standard.setValue(LoginDict, forKey: "LoginDict")
    }
    
    class func getLoginDetails() -> NSDictionary
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:NSDictionary! = userdefaults.object(forKey: "LoginDict") as? NSDictionary
        return userData
    }
    
    //  used in SmartSwitch
    class func saveUserBio(UserBio:String)
    {
        UserDefaults.standard.setValue(UserBio, forKey: "UserBio")
    }
    
    class func getUserBio() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "UserBio") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveHomeSSId(ID: String)  {
        UserDefaults.standard.setValue(ID, forKey: "SmartSwitchSSId")
    }
    class func getHomeSSId() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:String! = userdefaults.object(forKey: "SmartSwitchSSId") as! String?
        return userData
    }
    
    
    class func saveSaveSerialNo(ID: String)  {
        UserDefaults.standard.setValue(ID, forKey: "DeviceSerialNo")
    }
    class func getSerialNo() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:String! = userdefaults.object(forKey: "DeviceSerialNo") as! String?
        return userData
    }
    
    class func saveUserImage(UserImage: UIImage)  {
        // UserDefaults.standard.setValue(UIImageJPEGRepresentation(UserImage, 100)!, forKey: "key")
        UserDefaults.standard.setValue(UserImage.jpegData(compressionQuality: 100), forKey: "key")
        
    }
    class func getUserImage() -> Data?
    {
        
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:Data? = userdefaults.object(forKey: "key") as! Data?
        
        return userData
    }
    
    class func saveUserCoverImage(UserCoverImage: UIImage)  {
        UserDefaults.standard.setValue(UserCoverImage.jpegData(compressionQuality: 100), forKey: "UserCoverImage")
        
    }
    class func getUserCoverImage() -> Data
    {
        
        let userdefaults:UserDefaults = UserDefaults.standard
        let userData:Data! = userdefaults.object(forKey: "UserCoverImage") as! Data?
        
        return userData
    }
    
    class func savePassword(savePaasword:String)
    {
        UserDefaults.standard.setValue(savePaasword, forKey: "password")
    }
    
    class func getPassword() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "password") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveMobileNumber(saveNumber:String)
    {
        UserDefaults.standard.setValue(saveNumber, forKey: "mobileNumber")
    }
    
    class func getMobileNumber() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "mobileNumber") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    
    class func saveComapnyID(CompanyId:String)
    {
        UserDefaults.standard.setValue(CompanyId, forKey: "CompanyId")
    }
    
    class func getComapnyID() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "CompanyId") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func savePsID(PsId:String)
    {
        UserDefaults.standard.setValue(PsId, forKey: "PsId")
    }
    
    class func getPsId() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "PsId") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveWalletBalance(Balance:String)
    {
        UserDefaults.standard.setValue(Balance, forKey: "Balance")
    }
    
    class func getWalletBalance() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "Balance") as! String?
        if Userval == nil {
            return "0"
        }
        else{
            return Userval
        }
    }
    
    
    class func saveKYCStatus(Status:String)
    {
        UserDefaults.standard.setValue(Status, forKey: "kycstatus")
    }
    
    class func getKYCStatus() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "kycstatus") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    
    class func saveUserRole(Role:String)
    {
        UserDefaults.standard.setValue(Role, forKey: "role")
    }
    
    class func getUserRole() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "role") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
    
    class func saveWorkShopId(WorkShopId:String)
    {
        UserDefaults.standard.setValue(WorkShopId, forKey: "WorkShopId")
        
    }
    
    class func getWorkShopId() -> String
    {
        let userdefaults:UserDefaults = UserDefaults.standard
        let Userval:String! = userdefaults.object(forKey: "WorkShopId") as! String?
        if Userval == nil {
            return ""
        }
        else{
            return Userval
        }
    }
}


