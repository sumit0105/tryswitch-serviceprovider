//
//  UIView+Extenstion.swift
//  BalvahanApp
//
//  Created by subesh pandey on 04/11/20.
//

import Foundation
import UIKit

//import EMTNeumorphicView


//
//  UIView+Extenstion.swift
//  BalvahanApp
//
//  Created by subesh pandey on 04/11/20.
//

import Foundation
import UIKit




extension UIView {
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .white) {
        
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
    
    func setBorderColor(borderColor : UIColor){
        
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        
        
    }
    
    func setBorderColor(borderColor : UIColor, cornerRadiusBound: CGFloat){
        
        self.layer.masksToBounds = true
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = cornerRadiusBound
        
        
    }
    
    func setBorder_3D_View(){
        
        self.layer.shadowPath = nil
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.5
        
    }
    
    func clearConstraints() {
        for subview in self.subviews {
            subview.clearConstraints()
        }
        self.removeConstraints(self.constraints)
    }
    
    
    func setShadowColor(){
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4.0
    }
    
    func roundCorners(radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
//            layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            let rectShape = CAShapeLayer()
            rectShape.bounds = frame
            rectShape.position = center
            rectShape.path = UIBezierPath(roundedRect: bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
            layer.mask = rectShape
        }
    }
    
    func roundCornersTopright_BottomRight(radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = false
            layer.cornerRadius = radius
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        } else {
            let rectShape = CAShapeLayer()
            rectShape.bounds = frame
            rectShape.position = center
            rectShape.path = UIBezierPath(roundedRect: bounds,    byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: radius, height: radius)).cgPath
            layer.mask = rectShape
        }
    }
    
    func roundCornersBottomright_BottomLeft(radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = false
            layer.cornerRadius = radius
            layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            let rectShape = CAShapeLayer()
            rectShape.bounds = frame
            rectShape.position = center
            rectShape.path = UIBezierPath(roundedRect: bounds,    byRoundingCorners: [.bottomRight , .bottomLeft], cornerRadii: CGSize(width: radius, height: radius)).cgPath
            layer.mask = rectShape
        }
    }
    
    
    func topRoundCorner(radius: CGFloat, color:UIColor, borderWidth:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [ .layerMaxXMinYCorner,.layerMinXMinYCorner]
        self.layer.borderColor =  color.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    
    func bottomRoundCorner(radius: CGFloat, color:UIColor, borderWidth:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [ .layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        self.layer.borderColor =  color.cgColor
        self.layer.borderWidth = borderWidth
    }
    
    func constraint(byIdentifier identifier: String) -> NSLayoutConstraint? {
        return constraints.first(where: { $0.identifier == identifier })
    }
    @discardableResult func popIn(fromScale: CGFloat = 0.5,
                                  duration: TimeInterval = 0.5,
                                  delay: TimeInterval = 0.3,
                                  completion: ((Bool) -> Void)? = nil) -> UIView {
      isHidden = false
      alpha = 0
      transform = CGAffineTransform(scaleX: fromScale, y: fromScale)
      UIView.animate(
        withDuration: duration, delay: delay, usingSpringWithDamping: 0.55, initialSpringVelocity: 3,
        options: .curveEaseOut, animations: {
          self.transform = .identity
          self.alpha = 1
        }, completion: completion)
      return self
    }

    func addDashedBorder() {
        let color = UIColor.lightGray.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

        self.layer.addSublayer(shapeLayer)
        }
    
}

class CurveView:UIView {
    
    var once = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if once {
            
            let bb = UIBezierPath()
            
            bb.move(to: CGPoint(x: 0, y: self.frame.height))
            
            // the offset here is 40 you can play with it to increase / decrease the curve height
            
            bb.addQuadCurve(to: CGPoint(x: self.frame.width, y: self.frame.height), controlPoint: CGPoint(x: self.frame.width / 2 , y: self.frame.height + 100 ))
            
            bb.close()
            
            let l = CAShapeLayer()
            
            l.path = bb.cgPath
            
            l.fillColor =  self.backgroundColor!.cgColor
            
            self.layer.insertSublayer(l,at:0)
            
            once = false
        }
        
    }
    
}
@IBDesignable class RadialGradientView: UIView {

    @IBInspectable var outsideColor: UIColor = UIColor.red
    @IBInspectable var insideColor: UIColor = UIColor.green

    override func draw(_ rect: CGRect) {
        let colors = [insideColor.cgColor, outsideColor.cgColor] as CFArray
        let endRadius = sqrt(pow(frame.width/2, 2) + pow(frame.height/2, 2))
        let center = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let gradient = CGGradient(colorsSpace: nil, colors: colors, locations: nil)
        let context = UIGraphicsGetCurrentContext()

        context?.drawRadialGradient(gradient!, startCenter: center, startRadius: 0.0, endCenter: center, endRadius: endRadius, options: CGGradientDrawingOptions.drawsBeforeStartLocation)
    }
}
class GradientTabBarController: UITabBarController {

        let gradientlayer = CAGradientLayer()
        let uiimage = UIImageView()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: .white, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 3.0)
            self.selectedIndex = 2
            setGradientBackground(colorOne: UIColor(red: 73, green: 0, blue: 119),colorTwo: .red)
        }

        func setGradientBackground(colorOne: UIColor, colorTwo: UIColor)  {
            uiimage.frame = tabBar.bounds
            uiimage.frame = CGRect(x: tabBar.bounds.minX , y: tabBar.bounds.minY, width: tabBar.bounds.size.width , height: tabBar.frame.size.height + 20 )
//          uiimage.frame.size.width = tabBar.bounds.size.width + 20
            uiimage.image = UIImage(named: "TabbaFooter")
//            gradientlayer.colors = [colorOne.cgColor, colorTwo.cgColor]
//            gradientlayer.locations = [0, 1]
//            gradientlayer.startPoint = CGPoint(x: 1.0, y: 0.0)
//            gradientlayer.endPoint = CGPoint(x: 0.0, y: 0.0)
//            gradientlayer.cornerRadius = 10
            uiimage.clipsToBounds = true
            tabBar.clipsToBounds = true
            
           self.tabBar.roundCornersTopright_BottomRight(radius: 20)
            self.tabBar.addSubview(uiimage)
            
            self.tabBar.sendSubviewToBack(uiimage)
            
//            UITabBar.appearance().barTintColor = UIColor.clear
//            UITabBar.appearance().shadowImage = UIImage()
//            UITabBar.appearance().backgroundImage = UIImage(named: "TabbaFooter")
//            self.tabBar.layer.insertSublayer(gradientlayer, at: 0)
            
            
        }
}
extension UIImage {
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(x: 0, y: size.height - lineWidth, width: size.width, height: lineWidth))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
