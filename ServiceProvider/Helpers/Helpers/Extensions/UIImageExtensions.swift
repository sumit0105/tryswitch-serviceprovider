//
//  UImageExtensions.swift
//  Caviar
//
//  Created by DK on 21/08/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

extension FileManager {
    static var document:URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}

extension  UIImage {
    static func loadImage(_ url:URL) -> UIImage? {
        do {
            let imageData = try Data(contentsOf: url)
            return UIImage(data: imageData)
        } catch {}
        return nil
    }
    
    func save(_ url:URL) throws {
        if let imageData = self.pngData() {
            try imageData.write(to: url, options: .atomic)
        }
        else {
           
        }
    }
}

extension UIImage {
    func getImageRatio() -> CGFloat {
        let imageRatio = CGFloat(self.size.width / self.size.height)
        return imageRatio
    }
}
