//
//  AppleReceipt+Extension.swift
//  Caviar
//
//  Created by Ankur on 18/08/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

struct AppleReceipt {
    static var receipt:String? {
        guard let appStoreReceiptURL: URL = Bundle.main.appStoreReceiptURL else {return nil}
        if FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                return receiptString
            }
            catch {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                return nil
            }
        }
        return nil
    }
}

